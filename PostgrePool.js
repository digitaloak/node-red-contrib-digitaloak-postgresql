const EVENT = require("./events.js");
const STATUS = EVENT;
const { EventEmitter } = require("events");
const { Pool } = require("pg");

module.exports.PostgrePool = class PostgrePool {
    constructor(config) {
        this._config = config;
        this._event_emitter = new EventEmitter();
        this._event_emitter.setMaxListeners(200);
        this._is_connecting = false;
        this._scheduled_connect_ref = null;
        this._is_disconnecting = false;
        this._scheduled_disconnect_ref = null;
        this._subscribed_channels = [];

        this.pool = new Pool(this._config);
        this.pool.on('error', (err) => {
            if (this.status !== STATUS.POOL_DISCONNECTED) {
                this._event_emitter.emit(STATUS.POOL_DISCONNECTED, err);
                this.status = STATUS.POOL_DISCONNECTED;
            }
        });
        this.status = null;
    }

    on(topic, callback) {
        this._event_emitter.on(topic, callback)
    }

    removeListener(topic, callback) { this._event_emitter.removeListener(topic, callback) };

    query(query) { return this.pool.query(query) };
    
    async tx(query) { 
        const client = await this.pool.connect();
        try {
            let value_arr_idx;  // keeps current index of parameters array used
            let re = new RegExp(";(?=(?:[^']*'[^']*')*[^']*$)");
            let queries = query.text ? query.text.split(re) : query.split(re);    // query is valid as string and an object, depends if parameterized query; 
            let values = query.values ? query.values : [];  
            for (let i = 0; i < queries.length; i++) {  // iterate over all transaction queries
                let query = queries[i].trim();
                if (query.length === 0) continue;
                
                if (!values || values.length === 0 ) {  // if msg.values doesn't passed with transaction, query via client without params
                    await client.query(query);
                } else {    // if msg.values passed with transaction
                    if (query.match(/[^"'](\$[\d]+)[^"']/g)) {  // check if current query includes parameter (e.g. $1)
                        value_arr_idx = ++value_arr_idx || 0;   // change idx position on parameters array
                        await client.query(query, values[value_arr_idx]);  
                    } else {    // processed query doesn't contain any params
                        await client.query(query);
                    }   
                }
            }
            await client.query('COMMIT');
        } catch (e) {
            await client.query('ROLLBACK')
            throw e
        } finally {
            client.release()
        }
    };

    connect() {
        if (!this._scheduled_connect_ref && !this.is_disconnecting) {
            this._connect();
            this._scheduled_connect_ref = setInterval(() => this._connect(), this._config.reconnect_interval || 1000);
        }
    }

    async _connect() {
        if (!this._is_connecting && !this._is_disconnecting) {
            this._is_connecting = true;
            if (this.status !== STATUS.POOL_CONNECTED && this.status !== STATUS.POOL_CONNECTING) {
                this._event_emitter.emit(STATUS.POOL_CONNECTING);
                this.status = STATUS.POOL_CONNECTING;
            }
            try {
                await this.pool.query('SELECT 2');
                if (this.status !== STATUS.POOL_CONNECTED) {
                    this._event_emitter.emit(STATUS.POOL_CONNECTED);
                    this.status = STATUS.POOL_CONNECTED;
                }
                let pool_stats = {
                    "total_count": this.pool.totalCount,
                    "idle_count": this.pool.idleCount,
                    "waiting_count": this.pool.waitingCount
                }
                this._event_emitter.emit(EVENT.POOL_STATS, pool_stats);
            }
            catch (err) {
                if (this.status !== STATUS.POOL_CONNECTION_FAILED) {
                    this._event_emitter.emit(STATUS.POOL_CONNECTION_FAILED, err);
                    this.status = STATUS.POOL_CONNECTION_FAILED;
                }
            }
            finally {
                this._is_connecting = false;
            }
        }
    }

    disconnect(on_success) {
        if (!this._is_disconnecting) {
            this._is_disconnecting = true;
            if (this.status !== STATUS.POOL_DISCONNECTING) {
                this._event_emitter.emit(STATUS.POOL_DISCONNECTING);
                this.status = STATUS.POOL_DISCONNECTING;
            }
            clearInterval(this._scheduled_connect_ref);
            this._scheduled_connect_ref = null;
            this._is_connecting = false;
            this.pool.end()
                .then(() => {
                    if (this.status !== STATUS.POOL_DISCONNECTED) {
                        this.status = STATUS.POOL_DISCONNECTED;
                        this._event_emitter.emit(STATUS.POOL_DISCONNECTED);
                        on_success();
                    }
                })
                .catch(() => this.disconnect(on_success))
                .finally(() => this._is_disconnecting = false)
        }
    }
}

const { Client } = require("pg");
const EVENT = require("./events.js");
const STATUS = EVENT;
const { EventEmitter } = require("events");

module.exports.PostgreClient = class PostgreClient {

    constructor(config) {
        this._config = config;
        this._event_emitter = new EventEmitter();
        this._event_emitter.setMaxListeners(200);
        this._is_connecting = false;
        this._scheduled_connect_ref = null;
        this._is_disconnecting = false;
        this._scheduled_disconnect_ref = null;
        this._subscribed_channels = [];
        this.client = null;
        this.status = null;
    }
    
    async subscribe(channel_name) {
        this._subscribed_channels[channel_name] = this._subscribed_channels[channel_name] + 1 || 1;
        try {
            if (this._subscribed_channels[channel_name] === 1) {
                await this.client.query(`LISTEN ${channel_name}`);
                return true;
            }
        }
        catch (err) {
            throw `Cannot subscribe client to the channel '${channel_name}': ${err.message}.`;
        }
        return false;
    }

    async unsubscribe(channel_name) {
        if (typeof this._subscribed_channels[channel_name] === 'undefined' || this._subscribed_channels[channel_name] <= 0) throw `Client is not subscribed to the channel '${channel_name}'`;
        this._subscribed_channels[channel_name]--;
        if (this._subscribed_channels[channel_name] === 0) {
            try {
                await this.client.query(`UNLISTEN ${channel_name}`);
                delete this._subscribed_channels[channel_name];
                return true;
            } catch (err) {
                throw `Cannot unsubscribe client from the channel '${channel_name}': ${err.message}`;
            }
        }
        return false;
    }

    on(topic, callback) {
        this._event_emitter.on(topic, callback);
    }

    removeListener(topic, callback) { this._event_emitter.removeListener(topic, callback) }

    query(query) { return this.client.query(query) };

    connect(on_success) {
        if (this._scheduled_disconnect_ref) {
            clearInterval(this._scheduled_disconnect_ref);
            this._scheduled_disconnect_ref = null;
            this._is_disconnecting = false;
        }
        if (!this._scheduled_connect_ref && this.status !== STATUS.CLIENT_CONNECTED) {
            this._connect(on_success);
            this._scheduled_connect_ref = setInterval(() => this._connect(on_success), this._config.reconnect_interval || 3000);
        }
    }

    disconnect(on_success) {
        if (this._scheduled_connect_ref) {
            clearInterval(this._scheduled_connect_ref);
            this._scheduled_connect_ref = null;
            this._is_connecting = false;
        }
        if (!this._scheduled_disconnect_ref && this.status !== STATUS.CLIENT_DISCONNECTED && this.status !== STATUS.CLIENT_CONNECTION_FAILED) {
            this._disconnect(on_success);
            this._scheduled_disconnect_ref = setInterval(() => this._disconnect(on_success), 1000);
        } else {
            on_success();
        }
    }

    async _disconnect(on_success) {
        if (!this._is_disconnecting && !this._is_connecting) {
            this._is_disconnecting = true;
            if (this.status !== STATUS.CLIENT_DISCONNECTING) {
                this._event_emitter.emit(EVENT.CLIENT_DISCONNECTING);
                this.status = STATUS.CLIENT_DISCONNECTING;
            }
            try {
                await this.client.end();
                this.client = null;
                on_success();
                if (this.status !== STATUS.CLIENT_DISCONNECTED) {
                    this.status = STATUS.CLIENT_DISCONNECTED;
                    this._event_emitter.emit(EVENT.CLIENT_DISCONNECTED);
                }
                clearInterval(this._scheduled_disconnect_ref);
                this._scheduled_disconnect_ref = null;
            }
            catch (err) {
                if (this.status !== STATUS.CLIENT_DISCONNECTION_FAILED) {
                    this.status = STATUS.CLIENT_DISCONNECTION_FAILED;
                    this._event_emitter.emit(EVENT.CLIENT_DISCONNECTION_FAILED, err);
                }
            }
            finally {
                this._is_disconnecting = false;
            }
        }
    }

    async _connect(on_success) {
        if (!this._is_connecting && !this._is_disconnecting) {
            this._is_connecting = true;
            if (this.status !== STATUS.CLIENT_CONNECTING) {
                this._event_emitter.emit(EVENT.CLIENT_CONNECTING);
                this.status = STATUS.CLIENT_CONNECTING;
            }
            this.client = new Client(this._config);

            this.client.on('error', (err) => {
                if (this.status !== STATUS.CLIENT_DISCONNECTED) {
                    this._event_emitter.emit(EVENT.CLIENT_DISCONNECTED, err);
                    this.status = STATUS.CLIENT_DISCONNECTED;
                    this._subscribed_channels = {};
                }
                this.connect(on_success);
            });
            
            this.client.on('notification', (msg) => {
                this._event_emitter.emit(msg.channel, msg.payload);
            });

            try {
                await this.client.connect();
                await this.client.query('SELECT 2');

                if (this.status !== STATUS.CLIENT_CONNECTED) {
                    this._event_emitter.emit(EVENT.CLIENT_CONNECTED);
                    this.status = STATUS.CLIENT_CONNECTED;
                    on_success();
                }
                
                clearInterval(this._scheduled_connect_ref);
                this._scheduled_connect_ref = null;
            }
            catch (err) {
                if (this.status !== STATUS.CLIENT_CONNECTION_FAILED) {
                    this._event_emitter.emit(EVENT.CLIENT_CONNECTION_FAILED, err);
                    this.status = STATUS.CLIENT_CONNECTION_FAILED;
                }
            }
            finally {
                this._is_connecting = false;
            }
        }
    }
}

let events = {
    "POOL_CONNECTED": "pool_connected",
    "POOL_DISCONNECTED": "pool_disconnected",
    "POOL_NOT_CONNECTED": "pool_not_connected",
    "POOL_CONNECTING": "pool_connecting",
    "POOL_DISCONNECTING": "pool_disconnecting",
    "POOL_CONNECTION_FAILED": "pool_connection_failed",
    "POOL_DISCONNECTION_FAILED": "pool_disconnection_failed",
    "POOL_STATS": "pool_stats",
    "POOL_QUERY_EXECUTING": "pool_query_executing",
    "POOL_QUERY_EXECUTED": "pool_query_executed",
    "POOL_QUERY_ERROR": "pool_query_error",
    "CLIENT_CONNECTED": "client_connected",
    "CLIENT_DISCONNECTED": "client_disconnected",
    "CLIENT_NOT_CONNECTED": "client_not_connected",
    "CLIENT_LISTENING": "client_listening",
    "CLIENT_CONNECTING": "client_connecting",
    "CLIENT_DISCONNECTING": "client_disconnecting",
    "CLIENT_DISCONNECTION_FAILED": "client_disconnection_failed",
    "CLIENT_CONNECTION_FAILED": "client_connection_failed"
}

module.exports = Object.freeze(events);